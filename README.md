## Tutorial

You do your work, add some files, code etc., then push your changes with:

```bash
git add .
git commit -m "first commit"
git push
```

Now our changes are in main branch. Let's create a tag:

```bash
git tag v1.0.0                    # creates tag locally     
git push origin v1.0.0            # pushes tag to remote
```

If you want to delete the tag:

```bash
git tag --delete v1.0.0           # deletes tag locally    
git push --delete origin v1.0.0   # deletes remote tag
```

Patch version with commit:
```bash
npm version patch -m "Upgrade to %s for reasons"
```


Git log pretty:
```bash
git log --all --decorate --oneline --graph
```

see more https://medium.com/@pakin/git-%E0%B8%84%E0%B8%B7%E0%B8%AD%E0%B8%AD%E0%B8%B0%E0%B9%84%E0%B8%A3-git-is-your-friend-c609c5f8efea